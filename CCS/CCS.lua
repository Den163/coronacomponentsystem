if CCS == nil then
    CCS = {}
else
    return
end

getmetatable("").__add = function(leftString, rightString)
    return leftString..rightString
end

require "CCS.Types.Header"
require "CCS.Exceptions.Header"
require "CCS.Collections.Header"
require "CCS.Utils.Header"
require "CCS.Math.Header"
require "CCS.Components.Header"