CCS.Debug.Metrics = {}
local Metrics = CCS.Debug.Metrics

local startPoint = 0

function Metrics:setStart()
    startPoint = system.getTimer()
end

function Metrics:setEnd()
    local endTime = system.getTimer() - startPoint
    print("The time of selected area of code is: " + endTime + " ms")
    return endTime
end