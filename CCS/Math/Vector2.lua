local Object = CCS.Object

CCS.Vector2 = Object:extend()
local Vector2 = CCS.Vector2

function Vector2:constructor(x, y)    
    x = x or 0
    y = y or 0
    
    self.x = x
    self.y = y
end

function Vector2:zero()
    return Vector2( 0, 0 )
end

function Vector2:one()
    return Vector2( 1, 1 )
end

function Vector2:add(secondArgument)
    if type(secondArgument) == "number" then
        return Vector2:new( self.x + secondArgument, self.y + secondArgument )
    else
        return Vector2:new( self.x + secondArgument.x, self.y + secondArgument.y )
    end
end

function Vector2:divide(scalar)
    return Vector2:new( self.x / scalar, self.y / scalar )
end

function Vector2:multiply(scalar)
    return Vector2:new( self.x * scalar, self.y * scalar )
end

function Vector2:toString()
    return string.format( "(%.2f, %.2f)", self.x, self.y )
end

Vector2.__add = Vector2.add 
Vector2.__mul = Vector2.multiply 
Vector2.__div = Vector2.divide
Vector2.__tostring = Vector2.toString