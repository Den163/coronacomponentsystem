CCS.Object = {}
local Object = CCS.Object

function Object:new(...)
    local newObject = self:__createInstance()
    newObject:constructor(unpack(arg))
    
    return newObject
end

function Object:constructor(...)
end

function Object:extend()
    local extendedObject = self:__createInstance()
    
    return extendedObject
end

function Object:getType()
    return "Object" 
end

function Object:__createInstance()
    local newObject = {}
    
    self.__index = self
    setmetatable(newObject, self)
    
    return newObject
end

function Object:__newIndex(key, value)
    if key == "new" then
        error("SADDSA")
    else
        self[key] = value
    end
end

function Object:__call(...)
    return self:new(unpack(arg))
end

setmetatable(Object, 
    {
        __call = Object.__call
    }
)