local Exception = CCS.Exceptions.Exception

CCS.Exceptions.UnsupportedTypeException = Exception:extend()
local UnsupportedTypeException = CCS.Exceptions.UnsupportedTypeException

function UnsupportedTypeException:constructor(value)
    local valueType = (value.getType) and value:getType() or "N/A"
    
    self.message = string.format(
        "UnsupportedTypeException:\n" +
        "The type \"%s\" is not supported:\n" +
        "Lua type: %s, \nCCS type: %s ", 
        value, type(value), valueType
    )
end    


