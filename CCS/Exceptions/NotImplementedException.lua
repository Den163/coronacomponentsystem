local Exceptions = CCS.Exceptions
local Exception = Exceptions.Exception

Exceptions.NotImplementedException = Exception:extend()
local NotImplementedException = Exceptions.NotImplementedException

function NotImplementedException:constructor(message)]
    message = message or ""
    self.message = "NotImplementedException:\n" + message
end