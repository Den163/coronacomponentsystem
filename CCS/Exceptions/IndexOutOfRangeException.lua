local Exception = CCS.Exceptions.Exception

CCS.Exceptions.IndexOutOfRangeException = Exception:extend()
local IndexOutOfRangeException = CCS.Exceptions.IndexOutOfRangeException

function IndexOutOfRangeException:constructor(minValue, maxValue, curValue)
    self.message = string.format(
        "IndexOoutOfRangeException:\n " + 
        "current value: %i < min value: %i \n" +
        "current value: %i > max value: %i",
        curValue, minValue, curValue, maxValue
    )
end