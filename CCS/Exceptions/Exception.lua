local Object = CCS.Object
local Exceptions = CCS.Exceptions

Exceptions.Exception = Object:extend()
local Exception = Exceptions.Exception

function Exception:constructor(message)
    message = message or ""
    self.message = "Exception:\t" + message
end

function Exception:throw()
    error( 
        "\n***********************************************************************\n" +
        self.message + 
        "\n***********************************************************************")
end

function Exception:getType()
    return "Exception"
end