if CCS and CCS.Exceptions == nil then
    CCS.Exceptions = {}
else
    return
end

CCS.Exceptions.throw = function(exception)
    if exception.throw then exception:throw()
    else error("Thrown type must be exception")
    end
end

require "CCS.Exceptions.Exception"
require "CCS.Exceptions.NotImplementedException"
require "CCS.Exceptions.IndexOutOfRangeException"
require "CCS.Exceptions.UnsupportedTypeException"
require "CCS.Exceptions.UnsupportedOperationException"