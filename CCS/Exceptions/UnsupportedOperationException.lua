local Exception = CCS.Exceptions.Exception

CCS.Exceptions.UnsupportedOperationException = Exception:extend()
local UnsupportedOperationException = CCS.Exceptions.UnsupportedOperationException

function UnsupportedOperationException:constructor(message)
    message = message or ""
    self.message = "UnsupportedOperationException:\n" + message
end