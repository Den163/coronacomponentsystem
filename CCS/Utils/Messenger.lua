local Object = CCS.Object

CCS.Messenger = Object:extend()
local Messenger = CCS.Messenger

function Messenger:constructor()
    self.listeners = {}
end

function Messenger:addListener( name, func )
    self.listeners[name] = func
end

function Messenger:broadcast( name, ...)
    local listener = self.listeners[name]
    
    if listener then return listener(unpack(arg)) end
end

function Messenger:removeListener( name )
    self.listeners[name] = nil
end

function Messenger:isListenerExists(name)
    if self.listeners[name] then 
        return true
    else 
        return false
    end
end