local Object = CCS.Object
local Transform = CCS.Transform
local Messenger = CCS.Messenger
local Exception = CCS.Exceptions.Exception

CCS.GameObject = Object:extend()
local GameObject = CCS.GameObject

function GameObject:constructor()
    self.components = {}
    self.messenger = Messenger:new()
    self:addTransform( Transform:new( self.messenger ) )
end

function GameObject:addTransform( transform )
    self:addComponent( "Transform", transform )
    self.transform = transform
    
    transform:update()
end

function GameObject:addRenderer( renderer )
    self:addComponent( "Renderer", renderer )
    
    if self.transform then
        self.transform:update()
    end
end

function GameObject:addComponent(name, component)
    self.components[name] = component
end

--------------------------------- **GETTERS** -----------------------------------
function GameObject:getComponent(name)
    local component = self.components[name]
    
    if component then return component end
    
    Exception:new("There is no such component: " + name):throw()
end

function GameObject:getMessenger()
    return self.messenger
end

function GameObject:getType()
    return "GameObject"
end

--------------------------------- **SETTERS** -----------------------------------