local Object = CCS.Object
local MessengerEvents = CCS.GameObjectEvents
local Vector2 = CCS.Vector2

CCS.Transform = Object:extend()
local Transform = CCS.Transform


function Transform:constructor(messenger)
    self.__mesenger = messenger
    self.__metadata = {}
    
    self.__metadata.position = Vector2:zero()
    self.__metadata.scale = Vector2:one()
    
    self.__metadata.rotation = 0
    
    self.__metadata.z = 0
end

function Transform:update()
    self:setPosition( self:getPosition() )
    self:setScale( self:getScale() )
    self:setRotation( self:getRotation() )
    self:setZ( self:getZ() )
end

-------------------------- **GETTERS** -------------------------------------

function Transform:getPosition()
    local position = self.__metadata.position
    
    return Vector2(position.x, position.y)
end

function Transform:getRotation()
    return self.__metadata.rotation
end

function Transform:getScale()
   local scale = self.__metadata.scale
   return Vector2(scale.x, scale.y)
end

function Transform:getScaleX()
    return self.__metadata.scale.x
end

function Transform:getScaleY()
    return self.__metadata.scale.y
end

function Transform:getX()
    return self.__metadata.position.x
end

function Transform:getY()
    return self.__metadata.position.y
end

function Transform:getZ()
    return self.__metadata.z
end

-------------------------- **SETTERS** -------------------------------------

function Transform:setPosition(arg1, arg2)
    if type(arg1) == "table" then
        self.__metadata.position = arg1
        self.__mesenger:broadcast( MessengerEvents.POSITION_CHANGED, 
            arg1.x, arg1.y )
    else
        self:setX(arg1)
        self:setY(arg2)
    end
end

function Transform:setRotation(angle)
    self.__metadata.rotation = angle
    self.__mesenger:broadcast( MessengerEvents.ROTATION_CHANGED, angle )
end

function Transform:setScale(scaleVector2)
    self.__metadata.scale = scaleVector2
    self.__mesenger:broadcast( MessengerEvents.SCALE_CHANGED, scaleVector2.x, scaleVector2.y )
end

function Transform:setScaleX(x)
    self.__metadata.scale.x = x
    self.__mesenger:broadcast( MessengerEvents.SCALE_CHANGED, x )
end

function Transform:setScaleY(y)
    self.__metadata.scale.y = y
    self.__mesenger:broadcast( MessengerEvents.SCALE_CHANGED, nil, y )
end

function Transform:setX(x)
    self.__metadata.position.x = x
    self.__mesenger:broadcast( MessengerEvents.POSITION_CHANGED, x )
end

function Transform:setY(y)
    self.__metadata.position.y = y
    self.__mesenger:broadcast( MessengerEvents.POSITION_CHANGED, nil, y )
end

function Transform:setZ(z)
    self.__metadata.z = z
    self.__mesenger:broadcast( MessengerEvents.LAYER_CHANGED, z )
end