local Object = CCS.Object
local MessengerEvents = CCS.GameObjectEvents

CCS.Renderer = Object:extend()
local Renderer = CCS.Renderer

function Renderer:constructor(view, messenger)
    self.view = view
    self.messenger = messenger
    self:subscribe()
end

function Renderer:updatePosition(x, y)
    self.view.x = x or self.view.x
    self.view.y = y or self.view.y
end

function Renderer:updateScale(x, y)
    self.view.xScale = x or self.view.xScale
    self.view.yScale = y or self.view.yScale
end

function Renderer:updateRotation(angle)
    self.view.rotation = angle or self.view.rotation
end

function Renderer:subscribe()
    self.messenger:addListener( MessengerEvents.POSITION_CHANGED, 
        function(x, y) self:updatePosition(x, y)  end
    )
    
    self.messenger:addListener( MessengerEvents.SCALE_CHANGED,
        function(x, y) self:updateScale(x, y) end
    )
    
    self.messenger:addListener( MessengerEvents.ROTATION_CHANGED,
        function(angle) self:updateRotation(angle) end
    )
end