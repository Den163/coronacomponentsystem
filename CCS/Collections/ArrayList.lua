local Object = CCS.Object
local Collections = CCS.Collections
local UnsupportedTypeException = CCS.Exceptions.UnsupportedTypeException
local IndexOutOfRangeException = CCS.Exceptions.IndexOutOfRangeException
local ArrayIterator = CCS.Collections.ArrayIterator

Collections.ArrayList = Object:extend()
local ArrayList = Collections.ArrayList

function ArrayList:constructor(collection)
    if collection and collection.forEach then
        self.__data = {}
        self.__count = collection:getCount()
        
        for value in collection:forEach() do
            self:add(value)
        end
    elseif collection then
        self.__data = collection
        self.__count = #collection
    else
        self.__data = {}
        self.__count = 0
    end

    self.__iterator = ArrayIterator:new(self.__data)
end

function ArrayList:add(value)
    table.insert(self.__data, value)
    
    self.__count = self.__count + 1
end

function ArrayList:remove(value)
    local index = table.indexOf(self.__data, value)
    table.remove(self.__data, index)
    
    self.__count = self.__count - 1
end

function ArrayList:forEach()
    local iterator = self.__iterator
    
    return function()
        if iterator:moveNext() then
            return iterator:getCurrent()
        end
    end
end

function ArrayList:sort(sortFunc)
    table.sort(self.__data, sortFunc)
end

function ArrayList:__index(key)
    print(key)
    if type(key) == "number" and 
       (key < 1 or key > self.__count) then
           IndexOutOfRangeException:new(key):throw()
    end
    
    return self.__data[key]
end

function ArrayList:__newindex(key, value)
    if type(key) ~= "number" then 
        if key == "__data" or key == "__count" or
           key == "__iterator"
        then
            rawset(self, key, value)
            return
        end
        
        UnsupportedTypeException:new(key):throw()
    end
    
    if key < 1 or key > self.__count then
        IndexOutOfRangeException:new(1, self:getCount(), key):throw()
    end
    
    self.__data[key] = value
end

------------------------------ **GETTERS** --------------------------------

function ArrayList:getIterator()
    return self.__iterator
end

function ArrayList:getCount()
    return self.__count
end

------------------------------ **SETTERS** --------------------------------
