local Collections = CCS.Collections
local Object = CCS.Object
local NotImplementedException = CCS.Exceptions.NotImplementedException

Collections.BaseCollecton = Object:extend()
local BaseCollecton = Collections.BaseCollecton

-- @params
-- sortFunc {function) function(A, B) that returns true if A < B
function BaseCollecton:sort(sortFunc) 
    NotImplementedException:new():throw()
end

-- @returns {number} amount of all elements
function BaseCollecton:getCount()
    NotImplementedException:new():throw()
end

-- @returns {IEnumerator} enumerator of collection
function BaseCollecton:getIterator()
    NotImplementedException:new():throw()
end

-- @returns {function} function that returns next element every iteration
function BaseCollecton:forEach()
    NotImplementedException:new():throw()
end