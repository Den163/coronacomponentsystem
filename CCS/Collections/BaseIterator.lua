local Collections = CCS.Collections
local NotImplementedException = CCS.Exceptions.NotImplementedException

Collections.BaseIterator = CCS.Object:extend()
local BaseIterator = Collections.BaseIterator

-- @returns {bool} true if there is a next element and false if not
function BaseIterator:moveNext() 
    NotImplementedException:new():throw()
end

-- @returns {Object} current element in collection
function BaseIterator:getCurrent() 
    NotImplementedException:new():throw()
end