if CCS and CCS.Collections == nil then
    CCS.Collections = {}
else
    return
end

require "CCS.Collections.BaseIterator"
require "CCS.Collections.BaseCollection"
require "CCS.Collections.ArrayIterator"
require "CCS.Collections.ArrayList"