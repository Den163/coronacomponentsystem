local Object = CCS.Object

CCS.Collections.ArrayIterator = Object:extend()
local ArrayIterator = CCS.Collections.ArrayIterator

function ArrayIterator:constructor(data)
    self.__data = data
    self.__position = 0
end

function ArrayIterator:moveNext()
    self.__position = self.__position + 1
    
    if self.__position <= #self.__data then
        return true
    end
    
    return false
end

function ArrayIterator:getCurrent()
    return self.__data[self.__position]
end